# Rezeptnotizen und Einkaufsliste - Heiligabend 2022

- Vorspeise

  - Apfel-Meerrettich-Frischkäse
  - Bayerisches Bohnen-Hummus mit Sojasprossen
  - Crema di pomodoro del Lago di Coma

- Hauptgang
  - Lachsfilet auf Gemüsebett

## Gesamtzutaten

- 1 Laib münsterl. Brot

- 200 g Sonnenblumenkerne
- 200 g getrocknete Tomaten
- 4 EL Tomatenmark
- 1 TL Kräuter der Provence
- Prise Chiliflocken nach Belieben
- Gemüsebrühe

- 1 Apfel
- 1 Bio Zitrone
- 2-3 cm frischer Meerrettich
- 150 g Frischkäse
- 250 g Quark, 20 % Fett

- Olivenöl
- 3 Knoblauchzehen grob gehackt
- 1 Dose schwarze Bohnen abgetropft und abgespült (400 g)
- 1 Glas Tahina/Sesammus
- 2 EL Zitronensaft
- 1 Msp Kreuzkümmel gemahlen
- 1 Dose Kichererbsen
- Sojasprossen, selbst angepflanzt

- 1 Seite Lachs
- 1 Bund Dill
- 1000 g festk. Kartoffeln
- 500 g Möhren
- 1 Sellerieknolle
- 1 Zucchini
- 2 Zwiebeln
- 1 Schalotte
- 2 Tomaten
- 1 grosse Stange Lauch
- 1 gelbe Paprika
- 600 g TK Brokkoli
- Etwas Weißweinessig
- 1 l trockener, weißer Traubensaft

### Notizen

- Die Sonnenblumenkerne mind. 3 Std., am besten aber über Nacht, in einem Gefäß bedeckt mit Wasser quellen lassen. Kerne auf einem Sieb abtropfen lassen und in ein hohes Gefäß (z.B. Rührbecher) geben. Tomaten in kleine Stücke schneiden und zu den Sonnenblumenkernen geben. 2 EL des Öls, Tomatenmark sowie die Kräuter zugeben. Mit dem Pürierstab alles fein pürieren Gemüsebrühe nach und nach zufügen. Mit Salz und Pfeffer abschmecken. Source: https://www.einfachbacken.de/rezepte/veganer-brotaufstrich-3-schnelle-varianten

- https://proveg.com/de/vegane-rezepte/hummus-aus-schwarzen-bohnen/
