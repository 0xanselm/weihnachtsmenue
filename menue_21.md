# Rezeptnotizen und Einkaufsliste - Heiligabend 2022

- Vorspeise
    - Matjes Hausfrauenart auf roter Beete Carpaccio
    - Räucherlachs auf westfälischem Pumpernickel mit Meerrettichschaum
    - Büsumer Krabbensalat mit Einschlag auf Chikoreeschiffchen

- Dessert
    - Lebkuchenparfait mit rosmarineküsschen Apfelkompott und Weihnachtswallnuss

## Gesamtzutaten
+ 1 Bund Frühlingszwiebel
+ 1 saures Äpfelchen (vll aus dem Garten)
+ 4 Äpfel
+ 2 Bund Radieschen
+ 1 kleine rote Zwiebel
+ 2 Bund Dill
+ 1 Avocado
+ 1/4 Salatgurke
+ 2-3 Chikoree
+ Rosmarin
+ 2 großen Bio-Zitronen
+ 2-3cm frischer Meerrettich
+ Eingeschweißte und gekochte rote Beete (nicht die Scheibchen im Glaserl)
+ 100g Wallnüsse
+ 1 Glas Gewürzgurke
+ 4-6 Bio-Eier (Größe M)
+ 500 g Joghurt ~10% 
+ 600 g Schlagsahne
+ 2 Esslöffel Mayonnaise 
+ 1 EL Natives Olivenöl
+ 1 Säckchen rote Pfefferkörner
+ 4-6 Matjesfilets
+ 300 g Nordseekrabben
+ 1 Pck. Vanillin-Zucker
+ 300 g	Zucker
+ 150 g	Lebkuchen
+ Pumpernickel
+ Apfelsaft

### 1a. Matjes Hausfrauenart auf roter Beete Carpaccio
+ 1 Frühlingszwiebel
+ 1 Apfel
+ 1 Glas Gewürzgurke
+ 1 Bund Radieschen
+ Eingeschweißte und gekochte rote Beete
+ 500 g Joghurt ~10% 
+ 1 Säckchen rote Pfefferkörner
+ 2 Esslöffel Mayonnaise 
+ 4-6 Matjesfilets
+ 1 Bund Dill

### 1b. Räucherlachs auf westfälischem Pumpernickel mit Meerrettichschaum
+ 200 g	Schlagsahne
+ 2cm Frischer Meerrettich
+ saures Äpfelchen
+ Pumpernickel, gerne rechteckig
+ Sahne in einer Schüssel steif schlagen.
+ Meerretich an der Zitronenraffel dazureiben, mischen.
+ Apfel dazureiben, sofort mischen.
+ Zitronensaft und Salz und Pfeffer würzen.
+ Link:
    - https://www.kochbar.de/rezept/114853/Meerrettichschaum.html

### 1c. Büsumer Krabbensalat mit Einschlag auf Chikoreeschiffchen
+ 300 g Nordseekrabben
+ 1 Kleine rote Zwiebel
+ 1 Bund Radieschen 
+ 1 Avocado
+ 1 Chikoree
+ 1/4 Salatgurke
+ 2 EL Frischer Dill
+ 1 EL Natives Olivenöl
+ Saft und Zeste von einer großen Bio-Zitrone
+ Meersalz und frisch gemahlener schwarzer Pfeffer, nach Geschmack
+ Zitronenschnitten, zum Servieren (Optional)
+ Link:
    - https://ellerepublic.de/buesumer-krabbensalat/

### 2. Lebkuchenparfait mit rosmarineküsschen Apfelkompott und Weihnachtswallnuss
+ 4	Eier (Größe M)
+ 300 g	Zucker
+ 1 Pck. Vanillin-Zucker
+ 200 g	Schlagsahne
+ 200 g	kalte Schlagsahne
+ 150 g	Lebkuchen
+ 100g Wallnüsse
+ 3 Äpfel
+ Apfelsaft
+ Rosmarin
+ Link:
    - 14:20 https://www.3sat.de/kultur/esskulturen/hessen-a-la-carte-adventsrezepte-vom-land-100.html
